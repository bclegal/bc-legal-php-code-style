# Adaptation of Symphony Code style for BC Legal

```
phpcs --config-set installed_paths vendor/bclegal/bc-legal-php-code-style
```

To verify it is installed do:

```
phpcs -i
```

And you should see the 'bcl' listed as a code style
